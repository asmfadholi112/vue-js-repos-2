import router from "@/routes";
import firebase from "firebase";
// eslint-disable-next-line
import { db } from "@/services/firebaseSetup";

export default function auth({ next }) {
  firebase.auth().onAuthStateChanged(user => {
    if (!user) {
      return router.replace("/login");
    } else {
      return next();
    }
  });
}
