import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";
import auth from "@/routes/middlewares/auth";

export default [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard,
        meta: {
          middleware: [auth]
        }
      },
      {
        path: "user",
        name: "User Profile",
        component: UserProfile,
        meta: {
          middleware: [auth]
        }
      },
      {
        path: "table",
        name: "Table List",
        component: TableList,
        meta: {
          middleware: [auth]
        }
      },
      {
        path: "typography",
        name: "Typography",
        component: Typography,
        meta: {
          middleware: [auth]
        }
      },
      {
        path: "icons",
        name: "Icons",
        component: Icons,
        meta: {
          middleware: [auth]
        }
      },
      {
        path: "maps",
        name: "Maps",
        meta: {
          hideFooter: true,
          middleware: [auth]
        },
        component: Maps
      },
      {
        path: "notifications",
        name: "Notifications",
        component: Notifications,
        meta: {
          middleware: [auth]
        }
      },
      {
        path: "upgrade",
        name: "Upgrade to PRO",
        component: UpgradeToPRO
      }
    ]
  }
];
