import firebase from "firebase";
// eslint-disable-next-line
import { db } from "@/services/firebaseSetup";
import router from "@/routes";
import UserProfile from "@/pages/UserProfile.vue";
import Login from "@/pages/Login.vue";

export default [
  {
    path: "/login",
    component: Login,
    beforeEnter: (to, from, next) => {
      firebase.auth().onAuthStateChanged(user => {
        if (!user) {
          next();
        } else {
          router.replace("/user");
        }
      });
    }
  },
  {
    path: "/logout",
    component: UserProfile,
    beforeEnter: () => {
      firebase
        .auth()
        .signOut()
        .then(() => {
          router.replace("/login");
        });
    }
  }
];
