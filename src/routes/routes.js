import routerDashboard from "./modules/routerDashboard";
import routerAuthenticaton from "./modules/routerAuthenticaton";

export default [...routerDashboard, ...routerAuthenticaton];
