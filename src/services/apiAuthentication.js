import firebase from "firebase";
// eslint-disable-next-line
import { db } from "@/services/firebaseSetup";
import router from "@/routes";

export default {
  methods: {
    async loginAuthentication(req) {
      try {
        await firebase
          .auth()
          .signInWithEmailAndPassword(req.email, req.password);
        router.replace("/user");
      } catch (e) {
        throw e;
      }
    },

    async logoutAuthentication() {
      try {
        await firebase.auth().signOut();
        router.replace("/login");
      } catch (e) {
        throw e;
      }
    },

    async loginWithGoogle() {
      try {
        var provider = new firebase.auth.GoogleAuthProvider();
        await firebase.auth().signInWithPopup(provider);
        router.replace("/user");
      } catch (e) {
        throw e;
      }
    },

    async loginWithFacebook() {
      try {
        var provider = new firebase.auth.FacebookAuthProvider();
        await firebase.auth().signInWithPopup(provider);
        router.replace("/user");
      } catch (e) {
        throw e;
      }
    },

    async loginWithLinkedin() {
      try {
        // var provider = new firebase.auth.FacebookAuthProvider();
        await window.open(
          `https://${
            process.env.VUE_APP_FIREBASE_PROJECT_ID
          }.firebaseapp.com/popup.html`,
          "name",
          "height=585,width=400"
        );
        console.log("sadusuadsa");
        await firebase.auth().onAuthStateChanged();
        console.log("sadusuadsa");
        router.replace("/user");
        console.log("sadusuadsa");
      } catch (e) {
        throw e;
      }
    }
  }
};
